const path = require('path');
const jimp = require('jimp');
const fs = require('fs')

async function imageMosaic() {


    const mosaicPath = path.join(__dirname, `./moImg/${Math.floor(Math.random() * 12)}.png`);
    const imageMosaic = await jimp.read(mosaicPath);

    const _imgPath = path.join(__dirname, `./oriImg/${1510}.png`);
    const image = await jimp.read(_imgPath);

    image.composite(imageMosaic, 0, 0, {
        mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1.0, opacitySource: 0.7
    })

    await image.writeAsync('test107.png');
}


async function creaatePreMosaic(id) {
    let imageMosaic = await new jimp(1024, 1024, 0x0);

    let tileSize = 16
    let loopSize = 1024/tileSize
    for (let x=0; x<loopSize; x++){
        for (let y=0; y<loopSize; y++){
            // console.log(x*tileSize , y*tileSize)
            const tilePath = path.join(__dirname, `./oriImg/${Math.floor(Math.random() * 2999)}.png`);
            const tileImg = await jimp.read(tilePath);
            tileImg.resize(tileSize, tileSize)
            imageMosaic.composite(tileImg, x*tileSize, y*tileSize, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })

        }//end for
    }//end fot


    await imageMosaic.writeAsync(`./moImg/${id}.png`);
    // console.log(imageMosaic)
}


async function looping() {
    for (let id=100; id<200; id++){
        console.log("Do : ",id)
        await creaatePreMosaic(id)
    }
}

looping()