#!/bin/sh

PROJECT_ID="warden-closet"
SERVICE_NAME="warden-closet-server"
REGION="asia-southeast1"

# exit when any command fails
set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
exitCode=$?
if [ $exitCode -ne 0 ]; then
      trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT
fi

# Get the version from git commit hash
VERSION=$(git rev-parse HEAD)

# Note: enforce platform to linux/amd64 for build from M1 Macbook 
# ref: https://stackoverflow.com/a/68766137
docker build . -t asia.gcr.io/$PROJECT_ID/$SERVICE_NAME:$VERSION --platform linux/amd64
docker push asia.gcr.io/$PROJECT_ID/$SERVICE_NAME:$VERSION

gcloud run deploy $SERVICE_NAME \
      --image asia.gcr.io/$PROJECT_ID/$SERVICE_NAME:$VERSION \
      --no-allow-unauthenticated \
      --platform managed \
      --region $REGION \
      --allow-unauthenticated \
      --port=5000
